library(shiny)
library(tidyverse)
library(plotly)
library(shinycssloaders)
#require(devtools)
#install_github('ramnathv/rCharts',force = TRUE)
library(rCharts)

ui=fluidPage(
  titlePanel="Trabajo Inferencia Estadistica(Puntuación Jornadas)",
  sidebarLayout(
    sidebarPanel(
      h1(textOutput("Puntuación de los equipos de la liga santander en la temporada 2021-2022")),
      br(),
      helpText("Si selecciona la Jornada 0 (Por defecto) se observara un grafico animado de
               la puntuación jornada a jornada. En caso contraria se observara unicamente los puntos de cada equipo en la jornada seleccionada"),
      numericInput("Jornada","Seleccione Jornada:",value=0,min=0,max=38),
      actionButton("Actualizar","Actualiza Información")
      
    ),
    mainPanel(
      h3("Puntuaciónes Temporada 2021-2022 Liga Santander"),
      plotlyOutput("grafico")
    )
  )
)

server=function(input,output,session){
  jornada=eventReactive(input$Actualizar,{
    dist=input$Jornada
  })
output$grafico=renderPlotly({
  if(jornada()==0){
    ggplot(PuntuacionJornada ,aes(x=Puntos,y=Equipo,color=Equipo))+geom_col() + guides(size="none")+theme(legend.position="none")+transition_time(Jornada) + labs(title="Jornada:{frame_time}")
  } else{
    p=ggplot(PuntuacionJornada %>% filter(Jornada == jornada()) %>% mutate(Equipo=fct_reorder(Equipo,Puntos)),aes(x=Puntos,y=Equipo,color=Equipo))+geom_col() + guides(size="none")+theme(legend.position="none")
    ggplotly(p)
    }
})
}


shinyApp(ui,server)