---
title: "Trabajo"
author: "Victor Toscano Duran"
date: "29/4/2022"
output:
  html_document: default
  pdf_document: default
---

Primer Paso: El primer paso del trabajo será el de realizar la importación de los archivos de datos que seran usados para realizar un análisis descriptivo y posteriormente un análisis estadístico que nos permita obtener conclusiones de intéres. Los datos con los que vamos a trabajar han sido descargados de la aplicación kaggle y abordan las estadisticas de los jugadores de futból durante los ultimos años(2012-2021) de las competiciones europeas,aunque en este trabajo trabajaremos unicamente con los datos de las 5 grandes ligas europeas, asi como su valor de mercado en diferentes fechas y principalmente con los datos de la última temporada: 2021-2022

Son varios archivos con los que trabajaremos:

-   Clubs.csv donde se muestra la información principal de los clubles.

-   players.csv donde se muestra la información principal de los jugadores.

-   leagues.csv donde se muestra la información principal de las ligas.

-   appearances.csv donde se muestra la información principal de las apariciones de los futbolistas en los partidos.

-   games.csv donde se muestra las estadísticas de los partidos.

-   player_valuationcsv donde se muestra el valor de mercado de los jugadores.

-   competitions.csv información de las principales competiciones europeas.

Los archivos al ser todos csv ,serán cargados a traves de la librería tidyverse mediante la función read_csv y usaré alguno de sus argumentos para organizar estos conjunto de una manera más personal y más legible desde mi punto de vista.

```{r}
#Datos de final de temporada
library(tidyverse)
Equipos=read_csv("DatosActualizados/clubs.csv",col_types = cols(EquipoId=col_integer(),Equipo=col_character(),LigaId=col_factor(),ValorEquipo=col_double(),Plantilla=col_integer(),EdadMedia=col_double(),Extranjeros=col_integer(),Nacionales=col_integer(),Estadio=col_character(),CapacidadEstadio=col_integer(),MayorVenta=col_character(),Entrenador=col_character(),.default = col_skip()),col_names = c("EquipoId","Nombre","Equipo","LigaId","ValorEquipo","Plantilla","EdadMedia","Extranjeros","Pextranjero","Nacionales","Estadio","CapacidadEstadio","MayorVenta","Entrenador"),skip=1) 

Ligas=read_csv("DatosActualizados/leagues.csv",col_names=c("LigaId","Liga"),col_types = cols(LigaId=col_factor(),Liga=col_factor(),.default = col_skip()),skip=1)

Partidos=read_csv("DatosActualizados/games.csv",col_names=c("PartidoId","LigaId","Temporada","Jornada","Fecha","LocalId","VisitanteId","GolesLocales","GolesVisitantes","x","y","Estadio","Publico","Arbitro","z"),col_types = cols(.default=col_integer(),LigaId=col_factor(),Arbitro=col_factor(),Fecha=col_date(),Temporada=col_factor(),Jornada=col_character(),x=col_skip(),y=col_skip(),Estadio=col_factor(),z=col_skip()),skip=1)

ValorJugadores=read_csv("DatosActualizados/player_valuations.csv",col_types = cols(JugadorId=col_integer(),Fecha=col_date(),.default = col_double()),col_names = c("JugadorId","Fecha","ValorEuros"),skip=1)

Jugadores=read_csv("DatosActualizados/players.csv",col_types = cols(JugadorId=col_integer(),UltimaTemp=col_factor(),PaisNacimi=col_factor(),Nacionalidad=col_factor(),Posicion=col_factor(),Subposicion=col_factor(),Pie=col_factor(),Jugador=col_character(),ClubActual=col_integer(),FechaNac=col_date(),Altura=col_integer(),MayorValor=col_double(),ValorActual=col_double(),Jugador=col_character(),.default = col_skip()),col_names = c("JugadorId","UltimaTemp","ClubActual","Nombre","Jugador","PaisNacimi","Nacionalidad","FechaNac","Posicion","Subposicion","Pie","Altura","ValorActual","MayorValor"),skip=1)

Apariencias=read_csv("DatosActualizados/appearances.csv",col_names = c("JugadorId","PartidoId","x","LigaId","ClubId","G","A","Mins","Amarilla","Roja"),col_types=cols(.default=col_integer(),Amarilla=col_factor(),Roja=col_factor(),LigaId=col_factor(),x=col_skip()),skip=1)

Competicion=read_csv("DatosActualizados/competitions.csv",col_names=c("CompeticionId","Nombre","Tipo","PaisId","Pais","LigaId","x","y"),col_types=cols(.default=col_skip(),CompeticionId=col_factor(),LigaId=col_factor(),Nombre=col_factor(),Tipo=col_factor(),Pais=col_factor()),skip=1)
```

Una vez,tenemos todos los datos importados,procedemos a definir correctamente algunas de las variables de estos archivos de datos,para un facil reconocimiento e interpretación de ellos, asi como la creación de nuevas variables a partir de otras de ellas que nos faciliten el analisis que se llevará a cabo posteriormente.Además como solo vamos a fijarnos en las 5 grandes ligas de Europa,haremos un filtrado de todos estor archivos en cuestión que solo tengamos datos de las 5 grandes ligas.

```{r}
library(magrittr)
Jugadores %<>% separate(Subposicion,into=c("x","Subposicion"),sep=" - ") %>% mutate(Posicion=fct_recode(Posicion,
            "Portero"="Goalkeeper",
            "Defensa"="Defender",
            "Centrocampista"="Midfield",
            "Delantero"="Attack"),
 Subposicion=fct_recode(Subposicion,
                        "Mediocentro"="Central Midfield",
                        "Mediocentro Defensivo"="Defensive Midfield",
                        "Mediocentro ofensivo"="Attacking Midfield",
                        "Interior izquierdo"="Left Midfield",
                        "Interior derecho"="Right Midfield",
                        "Delantero Centro"="Centre-Forward",
                        "Extremo Izquierdo"="Left Winger",
                        "Extremo derecho"="Right Winger",
                        "Mediapunta"="Second Striker",
                        "Central"="Centre-Back",
                        "Lateral Izquierdo"="Left-Back",
                        "Lateral derecho"="Right-Back"),
 Pie=fct_recode(Pie,
                "Diestro"="Right",
                "Zurdo"="Left",
                "Ambidiestro"="Both")) %>% select(-x)

Jugadores %<>% mutate(Subposicion=fct_explicit_na(Subposicion,"Portero"))


grandesligas=c("Spain","Germany","Italy","England","France")
GrandesLigas=Competicion %>% filter(Pais %in% grandesligas & Tipo=="first_tier") %>% select(-Tipo) #first tier significa competición liguera,por lo tanto buscando las competiciones ligueras de los 5 paises europeos que queremos observar obtenemos las 5 grandes ligas europeas
GrandesLigas #ya tenemos las 5 grandes ligas y sus codigos de ligas.

#Vamos a seleccionar unicamente los datos de los 5 grandes ligas de todos los datasets,para no tener información que no queremos usar y puedan crearnos ruido posteriormente

Partidos %<>% filter(str_detect(Jornada,"^\\d")) %>% semi_join(GrandesLigas,by="LigaId") %>% separate(Jornada,into=c("Jornada","Ruido"),sep=". ") %>% select(-Ruido) %>% mutate(Jornada=as.integer(Jornada)) #seleccionamos unicamente las jornadas de las grandes 5 ligas


Apariencias %<>% semi_join(Partidos,by="PartidoId")

Equipos %<>% semi_join(GrandesLigas,by="LigaId") #todos los equipos que han pasado en estas temporadas por las 5 grandes ligas.

Jugadores %<>% semi_join(Equipos,by=c("ClubActual"="EquipoId"))




ValorJugadoresTemp2020=ValorJugadores %>% filter(Fecha >= "2021-06-01" & Fecha <= "2021-08-20") #Valor al acabar la temporada ya que entre el 1 de Junio y 15 de Julio no se juegan estan 5 competiciones ligueras



Mix=Apariencias %>% left_join(Partidos,by="PartidoId")
Mix %>% filter(LigaId.x=="ES1" & Temporada ==2021) %>% group_by(ClubId,JugadorId) %>% summarise(Goles=sum(G),Asis=sum(A),Minutos=sum(Mins)) %>% ungroup() %>% left_join(Jugadores,by="JugadorId") %>% select(ClubId,Jugador,Posicion,Goles,Asis,Minutos) %>% left_join(Equipos,by=c("ClubId"="EquipoId")) %>% select(Equipo,Jugador,Posicion,Goles,Asis,Minutos) %>% arrange(desc(Goles))
```

Realizamos un modelo de regresión para predecir el Valor que costará un jugador a final de temporada (2020-2021) según algunas estadisticas de la temporada,algunos datos personales y el equipo.

```{r message=FALSE, warning=FALSE}
Mix=Apariencias %>% left_join(Partidos,by="PartidoId")

ValorJugadoresTemp2020=ValorJugadores %>% filter(Fecha >= "2021-06-01" & Fecha <= "2021-08-20") #Valor al acabar la temporada ya que entre el 1 de Junio y 15 de Julio no se juegan estan 5 competiciones ligueras

Modelo = Mix %>% filter(Temporada==2021) %>% group_by(Temporada,JugadorId) %>% summarise(Goles=sum(G),Asis=sum(A),Minutos=sum(Mins)) %>% left_join(Jugadores,by="JugadorId") %>% left_join(Equipos,by=c("ClubActual"="EquipoId")) %>% select(Jugador,Equipo,Nacionalidad,FechaNac,Subposicion,Pie,Altura,Goles,Asis,Minutos,ValorActual) #Generamos el conjunto de datos especificos para generar el modelo de predicción

library(tidymodels)
Modelo_split=Modelo %>% initial_split(prop=0.8) #creamos la partición de los datos
Modelo_recipe=training(Modelo_split) %>% recipe(ValorActual~Goles+Asis+Minutos+Nacionalidad+FechaNac+Subposicion+Pie+Altura+Equipo) %>% step_center(c("Goles","Asis","Minutos","Altura")) %>% step_scale(c("Goles","Asis","Minutos","Altura")) %>% step_dummy(c("Nacionalidad","Subposicion","Equipo")) %>% step_log("ValorActual")  %>% prep() #detallamos el preprocesamiento de los datos 
training(Modelo_split)
Modelo_training_preprocesado=bake(Modelo_recipe,new_data=NULL)
Modelo_testing_preprocesado <- Modelo_recipe %>% bake(testing(Modelo_split)) 
Modelo_rl=linear_reg() %>% set_engine("glm") %>% set_mode("regression") %>% fit(ValorActual~.,data=Modelo_training_preprocesado) #aplicamos el modelo a los datos para tener un ajuste

predict(Modelo_rl,Modelo_testing_preprocesado) %>% bind_cols(Modelo_testing_preprocesado) %>% metrics(truth=ValorActual,estimate=.pred) #calculamos las predicciones y las metricas.




```

Vamos a realizar un modelo de clasificación,para predecir la planta a traves del tratamiento,la cantidad de dioxido de carbon en el ambiente y la tasa de absorción del de dioxido de carbono.

No sabía como plantear un modelo de clasificación para los datos de mi trabajo,por ello lo realizo con un dataset ya creado que me ha parecido adecuado.

Vamos a realizar mediante un arbol (boost_tree) y uno de los motores que van de la mano con este tipo de arbol.

```{r message=FALSE, warning=FALSE}
library(datasets)
datos=CO2
skimr::skim(datos)

modelo_spec=boost_tree() %>% set_engine("xgboost") %>% set_mode("classification") #detallamos el modelo a usar para este ejemplo.

datos_split=datos %>% initial_split(prop=0.9,strata=Type) #realizamos strata para tener el mismo número de datos de ambos origenes.

datos_recipe = training(datos_split) %>%recipe(Plant~Treatment+conc+uptake) %>%
  step_corr(all_numeric()) %>%
  step_normalize(all_numeric()) %>% prep() #Realizamos el preprocesamiento
datos_test_preprocesados=datos_recipe %>% bake(testing(datos_split))
datos_ent_preprocesado=bake(datos_recipe,new_data=NULL)

AjusteModelo=modelo_spec %>% fit(Plant~.,data=datos_ent_preprocesado)
predict(AjusteModelo,datos_test_preprocesados) %>% bind_cols(datos_test_preprocesados) %>% metrics(truth=Plant,estimate=.pred_class) #tenemos unas medidas de rendimiento nefasta,que se preveen debido al poco numero de datos y la enorme cantidad de tipo de clases existentes de plantas.

probpredict=predict(AjusteModelo,datos_test_preprocesados,type="prob") %>% bind_cols(datos_test_preprocesados)

#Como no tenemos una planta para cada tipo de planta existente no se puede realizar todas las curva roc mediante la función que hemos vsito en clase de roc_curve + autoplot
```

Informacion general de cada equipo:Para ellos juntamos la información de los equipos y los jugadores y para que sea la información sobre el ultimo año filtramos por el 2021.

```{r}
InfJugadorEquipo=Jugadores %>% filter(UltimaTemp == 2021) %>% inner_join(Equipos,by=c("ClubActual"="EquipoId"))

#Realizado ya en shiny.R

#Alternativa a str_glue_datafor (i in 1:10){cat("El jugador,InfJugadorEquipo$Jugador[i], " es ", as.character(InfJugadorEquipo$Pie[i]))}
```

A continuación vamos a calcular la puntuacion por jornada de cada equipo y hacer un gráfico interactivo por jornada de la clasificación y sus puntos.(Equipo eje y (ordenados),puntos eje x y interactividad jornada. Lo haremos de la Liga española en esta ultima edición

```{r}
Partidos %>% filter(LigaId=="ES1" & Temporada==2021) %>% arrange(Jornada) %>% mutate(PuntosLocal=if(GolesLocales>GolesVisitantes){print(3)} else {if(GolesLocales==GolesVisitantes){print(1)}else{print(0)}} )
PartidosEspaña2021=Partidos %>% filter(LigaId=="ES1" & Temporada==2021) %>% arrange(Jornada)

PuntosLocal=c()

for (i in 1:nrow(PartidosEspaña2021)){
  if(PartidosEspaña2021$GolesLocales[i]>PartidosEspaña2021$GolesVisitantes[i]){PuntosLocal[i]=3} else {if(PartidosEspaña2021$GolesLocales[i]==PartidosEspaña2021$GolesVisitantes[i]){PuntosLocal[i]=1}else{PuntosLocal[i]=0}}}

PuntosVisitante=c()
for (i in 1:nrow(PartidosEspaña2021)){
  if(PartidosEspaña2021$GolesLocales[i]>PartidosEspaña2021$GolesVisitantes[i]){PuntosVisitante[i]=0} else {if(PartidosEspaña2021$GolesLocales[i]==PartidosEspaña2021$GolesVisitantes[i]){PuntosVisitante[i]=1}else{PuntosVisitante[i]=3}}}


#Ya tenemos la puntuacion que cada equipo gana por jornada,ahora debemos calcular la puntuacion acumulada por jornada.
PartidosEspaña2021Local = PartidosEspaña2021 %>% mutate(PuntosLocal=PuntosLocal,PuntosVisitante=PuntosVisitante) %>% group_by(LocalId) %>% arrange(LocalId) %>% mutate(Puntos=PuntosLocal,EquipoId=LocalId) %>% ungroup() %>% select(Jornada,EquipoId,Puntos)

PartidosEspaña2021Visitante= PartidosEspaña2021 %>% mutate(PuntosLocal=PuntosLocal,PuntosVisitante=PuntosVisitante) %>% group_by(VisitanteId) %>% arrange(VisitanteId) %>% mutate(Puntos=PuntosVisitante,EquipoId=VisitanteId) %>% ungroup() %>% select(Jornada,EquipoId,Puntos)

PuntuacionJornada=rbind(PartidosEspaña2021Local,PartidosEspaña2021Visitante)
PuntuacionJornada %<>% arrange(EquipoId,Jornada) %>% group_by(EquipoId) %>% summarise(Puntos=cumsum(Puntos)) %>% ungroup() %>% mutate(Jornada=rep(1:38,times=20)) %>% left_join(Equipos) %>% select(1:4) %>% mutate(Equipo=as.factor(Equipo))


library(gganimate) #graficos animados
ggplot(PuntuacionJornada ,aes(x=Puntos,y=Equipo,color=Equipo))+geom_col() + guides(size="none")+theme(legend.position="none")+transition_time(Jornada) + labs(title="Jornada:{frame_time}")




```

Ahora crearemos el estadios de futbol:

```{r}
ResultadosPartidos2021=PartidosEspaña2021 %>% left_join(Equipos,by=c("LocalId"="EquipoId")) %>% select(ends_with("Id"),contains("Goles"),EquipoLocal=Equipo,ValorEquipoLocal=ValorEquipo,EstadioLocal=Estadio.x) %>% left_join(Equipos,by=c("VisitanteId"="EquipoId")) %>% select(EquipoLocal,EquipoVisitante=Equipo,4:5,ValorEquipoLocal,ValorEquipoVisitante=ValorEquipo)


```

Graficos de asistencia a los estadios. En el dataset Partidos tenemos los partidos de las 5 grandes Ligas desde la temporada 2012 hasta 2021,donde viene recojida la asistencia de cada partido.

```{r}
library(lubridate)

#Numeros de partidos jugados en las 5 grandes Ligas cada Temporada por dia de Semana.
Partidos %>% group_by(LigaId,Temporada) %>% mutate(wday=wday(Fecha,label=TRUE)) %>% count(wday) %>% ggplot(aes(x=fct_reorder(wday,n),y=n,fill=n))+
  geom_col()+coord_flip() + ylab("Numero de partidos") + xlab("Dia de la semana") + facet_grid(LigaId ~ Temporada) +labs(title="Numero de partidos por dia de la semana") + theme_bw()

Partidos %>% filter(Temporada==2020 | Temporada==2021) %>%count(week=floor_date(Fecha,"week")) %>% ggplot(aes(week,n)) + geom_line() + xlab("Semana") + ylab("Numero de partidos") + labs(title="Numero de partidos por Semana",subtitle="Temporada 2020-2021 y 2021-2022") + theme_classic()

Partidos  %>% mutate(mes=month(Fecha,label=TRUE)) %>% group_by(Temporada,LigaId,mes) %>% summarise(publicomedio=mean(Publico)) %>% ggplot(aes(mes,publicomedio,fill=LigaId,color=mes)) + geom_col(position = "dodge") + facet_wrap(~Temporada) + labs(title="Asitencia Media",subtitle="Segun Mes y Liga")



#Podemos observar que tenemos bastanter datos faltantes para algunos de los años pero que todos los años siguen un mismo patron en cuanto a la asitencia media, por lo que nos quedaremos unicamente con el de 2015 ya que tiene datos correctos para todos los meses.

Partidos %>% filter(Temporada==2015)  %>% mutate(mes=month(Fecha,label=TRUE)) %>% group_by(LigaId,mes) %>% summarise(publicomedio=mean(Publico)) %>% ggplot(aes(mes,publicomedio,fill=LigaId,color=mes)) + geom_col(position = "dodge") +labs(title="Asitencia Media 2015",subtitle="Segun Mes y Liga")

Partidos %>% filter(Temporada==2021 & LigaId=="ES1" & Jornada==7) %>% group_by(Estadio) %>% summarise(PublicoTotal=sum(Publico)) %>%
  ggplot()+geom_bar(aes(x=Estadio,y=PublicoTotal,fill=Estadio),stat="identity",show.legend = FALSE,
    width = 1)+ coord_polar()
```

En esta sección, vamos a incluir algunos ejemplos de aplicación de funciones mediante la librería Purrr, ya que me cuesta usar estas técnicas por delante de otras a la hora de trabajar con los datos. Son simplemente para demostrar el conocimiento de esta librería,pero no se me han ocurrido ideas de como aplicarla de manera interesante al trabajo.
```{r}
Mix=Apariencias %>% left_join(Partidos,by="PartidoId")
estadis=Mix %>% filter(LigaId.x=="ES1" & Temporada ==2021) %>% group_by(ClubId,JugadorId) %>% summarise(Goles=sum(G),Asis=sum(A),Minutos=sum(Mins)) %>% ungroup() %>% left_join(Jugadores,by="JugadorId") %>% select(ClubId,Jugador,Posicion,Goles,Asis,Minutos) %>% left_join(Equipos,by=c("ClubId"="EquipoId")) %>% select(Equipo,Jugador,Posicion,Goles,Asis,Minutos)



map2_int(estadis$Goles,estadis$Asis,~.x+.y) # Calculamos el número totales de goles en los que han sido decisivos cada jugador de la liga española en esta temporada
map(estadis[,4:5],sum)   #Calculamos el número totales de goles en esta temproada y asistencias en la liga santander

lista=list(jugador=estadis$Jugador,goles=estadis$Goles,asis=estadis$Asis,minutos=estadis$Minutos)
pmap(lista,function(jugador,goles,asis,minutos) data.frame(nombre=jugador,goles=rnorm(mean=goles,n=5,sd=1),asis=rnorm(mean=asis,n=5,sd=1))) #generamos valores simulados de los goles y asistancias que van a marcar,se que es algo irreal pero no se me ocurre que hacer
pluck(lista,2,1)#seleccionamos el elemento de la primera fila,del segundo elemento de la lista

```


