# Portfolio



## Este es mi portafolio, donde están las muestras de trabajo realizadas por mi más destacables.

## This is my portfolio, where you can find the most outstanding samples of my work.


### Resumen 
En la carpeta TFG,podemos encontrar el proyecto acerca de mi Trabajo Fin de Grado "Indicadores asociados a la Encuesta de Condiciones de Vida", donde destaca el PDF y la "Presentación.Rmd", que es una aplicación interactiva acerca del trabajo que utilizé para exponerlo.

En la carpeta Inteligencia Artificial, podemos encontrar el proyecto en el que use diversas técnicas de la Inteligencia Artificial para elaborar finalmente una presentación interactiva en shiny. "Presentacion.Rmd".


En la carpeta Business Intelligence, podemos encontrar un proyecto realizado en PowerBI, en el cuál realizo un reporte en dicha aplicación.

En la carpeta Python, están subidas diferentes guías de las líbrerías que he aprendido en Python y de las cuáles he realizado cursos a tráves de DataCamp, con teoría y casos prácticos.

### Abstract 
In the TFG folder, we can find the project about my Final Degree Project "Indicadores asociados a la Encuesta de Condiciones de Vida", where we can find the PDF and the "Presentación.Rmd", which is an interactive application about the work that I used to present it.

In the Artificial Intelligence folder, we can find the project in which I used various Artificial Intelligence techniques to finally elaborate an interactive presentation in shiny. "Presentacion.Rmd".


In the Business Intelligence folder, we can find a project made in PowerBI, in which I made a report in this application.

In the Python folder, are uploaded different guides of the libraries that I have learned in Python and of which I have done courses through DataCamp, with theory and practical cases.

Translated with www.DeepL.com/Translator (free version)
